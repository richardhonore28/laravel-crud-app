<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //Table name
    protected $table = 'posts';
    //Primary Key
    public $primaryKey = 'id'; //This can be changed, just it need to be specified
    //Timestamps
    public $timestamps = true; //True by default

    public function user(){
    	return $this->belongsTo('App\User');
    }
}
